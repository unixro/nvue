=========================
nvidia.nvue Release Notes
=========================

.. contents:: Topics


v1.0.0
======

Release Summary
---------------

Initial release.

Major Changes
-------------

- nvidia.nvue.command
- nvidia.nvue.api

Minor Changes
-------------

- remove `debug.yml` playbook

Bugfixes
--------

